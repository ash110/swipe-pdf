import { Page, Text, View, Document, StyleSheet, Font } from '@react-pdf/renderer';
import { useState } from 'react';

Font.register({
    family: 'Open Sans',
    fonts: [
        { src: 'https://cdn.jsdelivr.net/npm/open-sans-all@0.1.3/fonts/open-sans-regular.ttf' },
        { src: 'https://cdn.jsdelivr.net/npm/open-sans-all@0.1.3/fonts/open-sans-600.ttf', fontWeight: 600 }
    ]
});

const styles = StyleSheet.create({
    page: {
        display: 'flex',
        flexDirection: 'column',
        height: '400px',
        width: '300px',
        border: '1px solid black'
    },
    topSection: {
        flex: 1,
        borderBottom: '1px solid black',
        display: 'flex',
        flexDirection: 'row',
    },
    topLeft: {
        flex: 1,
        borderRight: '1px solid black',
        display: 'flex',
        flexDirection: 'column',
        height: '50vh',
    },
    topLeftContainer: {
        flex: 1,
        borderBottom: '1px solid black',
    },
    topRight: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column'
    },
    topRightGrid: {
        display: 'flex',
        flexDirection: 'column',
    },
    gridRow: {
        display: 'flex',
        flexDirection: 'row',
        borderBottom: '1px solid grey',
        padding: '1px'
    },
    gridCell: {
        flex: 1,
        borderRight: '1px solid grey',
    },
    bottomSection: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
    },
    boldText: {
        fontWeight: 'bold',
        fontSize: '12px',
        fontFamily: 'Open Sans',
    },
    text: {
        fontSize: '10px',
        fontFamily: 'Open Sans',
    },
    table: {
        display: 'flex',
        flexDirection: 'column',
    },
    tableRow: {
        display: 'flex',
        flexDirection: 'row',
    },
    tableRowWithBorder: {
        display: 'flex',
        flexDirection: 'row',
    },
    tableRowColOneWithBorder: { flex: 1, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', borderBottom: '1px solid grey' },
    tableRowColTwoWithBorder: { flex: 18, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', borderBottom: '1px solid grey' },
    tableRowColThreeWithBorder: { flex: 6, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', borderBottom: '1px solid grey' },
    tableRowColFourWithBorder: { flex: 6, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', borderBottom: '1px solid grey' },
    tableRowColFiveWithBorder: { flex: 6, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', borderBottom: '1px solid grey' },
    tableRowColSixWithBorder: { flex: 6, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', borderBottom: '1px solid grey' },
    tableRowColSevenWithBorder: { flex: 2, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', borderBottom: '1px solid grey' },
    tableRowColEightWithBorder: { flex: 12, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', borderBottom: '1px solid grey' },
    tableRowColOne: { flex: 1, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', },
    tableRowColTwo: { flex: 18, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', },
    tableRowColThree: { flex: 6, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', },
    tableRowColFour: { flex: 6, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', },
    tableRowColFive: { flex: 6, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', },
    tableRowColSix: { flex: 6, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', },
    tableRowColSeven: { flex: 2, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', },
    tableRowColEight: { flex: 12, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'center', },
    belowTableTopText: {
        display: 'flex',
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    belowTable: {
        display: 'flex',
        flexDirection: "row",
    },
    belowTableRight: {
        flex: 1,
        width: "50wh",
    },
    belowTableLeft: {
        flex: 1,
        width: "50wh",
        height: "26.5vh",
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'stretch'
    },
    bankDetails: {
        display: 'flex',
        flexDirection: 'column',
    },
    bankDetail: {
        display: 'flex',
        flexDirection: 'row',
    },
    bankDetailsKey: {
        flex: 1,
        textAlign: 'left',
    },
    bankDetailsValue: {
        flex: 1,
        textAlign: 'left',
    },
    bottomLeftTable: {
        display: 'flex',
        border: '1px solid grey',
        height: '10vh',
        flexDirection: 'column',
        justifyContent: 'space-between',
        paddingRight: '5px'
    },
    bottomLeftTableTop: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    bottomLeftTableBottom: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
});


const items = [
    {
        slno: 1,
        item: "Asus 16.6 inch Monitor",
        hsn: 8471,
        due: "3 Days",
        rate: 7000,
        quantity: 10,
        per: "Nos",
        amount: 70000.00
    },
    {
        slno: 2,
        item: "Dell 17 inch Monitor",
        hsn: 8471,
        due: "3 Days",
        quantity: 10,
        rate: 8000,
        per: "Nos",
        amount: 80000.00
    }
]

const MyDocument = () => {
    return (
        <Document title="Sales Order">
            <Page size="A4" style={styles.page}>
                <View style={styles.pageLayout}>
                    {topView()}
                    {bottomView()}
                </View>
            </Page>
        </Document >
    );
}

const topView = () => (
    <View style={styles.topSection}>
        <View style={styles.topLeft}>
            <View style={styles.topLeftContainer}>
                <Text style={styles.boldText}>
                    Max Electronics
                </Text>
                <Text style={styles.text}>
                    {`Some Address
                                Some More Address
                                Contact
                                Email
                                Website`}
                </Text>
            </View>
            <View style={styles.topLeftContainer}>
                <Text style={styles.text}>Dispatch To</Text>
                <Text style={styles.boldText}>
                    Hi-Tech computer world
                </Text>
                <Text style={styles.text}>
                    {`Some Address
                                Some More Address
                                Contact
                                Email
                                Website`}
                </Text>
            </View>
            <View style={styles.topLeftContainer}>
                <Text style={styles.text}>Invoice To</Text>
                <Text style={styles.boldText}>
                    Hi-Tech computer world
                </Text>
                <Text style={styles.text}>
                    {`Some Address
                                Some More Address
                                Contact
                                Email
                                Website`}
                </Text>
            </View>
        </View>
        <View style={styles.topRight}>
            <View style={styles.topRightGrid}>
                <View style={styles.gridRow}>
                    <View style={styles.gridCell}>
                        <Text style={styles.text}>Voucher No.</Text>
                        <Text style={styles.boldText}>1</Text>
                    </View>
                    <View style={styles.gridCell}>
                        <Text style={styles.text}>Dated</Text>
                        <Text style={styles.boldText}>17-Mar-2020</Text>
                    </View>
                </View>
                <View style={styles.gridRow}>
                    <View style={styles.gridCell}>
                        <Text style={styles.text}></Text>
                        <Text style={styles.boldText}></Text>
                    </View>
                    <View style={styles.gridCell}>
                        <Text style={styles.text}>Mode of Payment</Text>
                        <Text style={styles.boldText}>Cheque</Text>
                    </View>
                </View>
                <View style={styles.gridRow}>
                    <View style={styles.gridCell}>
                        <Text style={styles.text}>Order No.</Text>
                        <Text style={styles.boldText}>1</Text>
                    </View>
                    <View style={styles.gridCell}>
                        <Text style={styles.text}>Other References</Text>
                    </View>
                </View>
                <View style={styles.gridRow}>
                    <View style={styles.gridCell}>
                        <Text style={styles.text}>Dispatched</Text>
                        <Text style={styles.boldText}>Road</Text>
                    </View>
                    <View style={styles.gridCell}>
                        <Text style={styles.text}>Destination</Text>
                        <Text style={styles.boldText}></Text>
                    </View>
                </View>
            </View>
            <Text style={styles.text}>Terms of Delivery</Text>
            <Text style={styles.boldText}>Ex Factory Delivery</Text>
        </View>
    </View>
)

const bottomView = () => (
    <View style={styles.bottomSection}>
        <View style={styles.table}>
            <View style={styles.tableRowWithBorder}>
                <View style={styles.tableRowColOneWithBorder}>
                    <Text style={styles.text}>
                        {`sl
                        no`}
                    </Text>
                </View>
                <View style={styles.tableRowColTwoWithBorder}>
                    <Text style={styles.text}>
                        Description of Goods
                    </Text>
                </View>
                <View style={styles.tableRowColThreeWithBorder}>
                    <Text style={styles.text}>
                        HSN/SAC
                    </Text>
                </View>
                <View style={styles.tableRowColFourWithBorder}>
                    <Text style={styles.text}>
                        Due on
                    </Text>
                </View>
                <View style={styles.tableRowColFiveWithBorder}>
                    <Text style={styles.text}>
                        Quantity
                    </Text>
                </View>
                <View style={styles.tableRowColSixWithBorder}>
                    <Text style={styles.text}>
                        Rate
                    </Text>
                </View>
                <View style={styles.tableRowColSevenWithBorder}>
                    <Text style={styles.text}>
                        per
                    </Text>
                </View>
                <View style={styles.tableRowColEightWithBorder}>
                    <Text style={styles.text}>
                        Amount
                    </Text>
                </View>
            </View>
            {
                items.map((it, index) => {
                    const { slno, item, hsn, due, quantity, rate, per, amount } = it;
                    return (
                        <View style={styles.tableRow}>
                            <View style={styles.tableRowColOne}>
                                <Text style={styles.text}>
                                    {slno}
                                </Text>
                            </View>
                            <View style={styles.tableRowColTwo}>
                                <Text style={styles.boldText}>
                                    {item}
                                </Text>
                            </View>
                            <View style={styles.tableRowColThree}>
                                <Text style={styles.text}>
                                    {hsn}
                                </Text>
                            </View>
                            <View style={styles.tableRowColFour}>
                                <Text style={styles.text}>
                                    {due}
                                </Text>
                            </View>
                            <View style={styles.tableRowColFive}>
                                <Text style={styles.boldText}>
                                    {`${quantity} ${per}`}
                                </Text>
                            </View>
                            <View style={styles.tableRowColSix}>
                                <Text style={styles.text}>
                                    {rate}
                                </Text>
                            </View>
                            <View style={styles.tableRowColSeven}>
                                <Text style={styles.text}>
                                    {per}
                                </Text>
                            </View>
                            <View style={index + 1 == items.length ? styles.tableRowColEightWithBorder : styles.tableRowColEight}>
                                <Text style={styles.boldText}>
                                    {amount}
                                </Text>
                            </View>
                        </View>
                    );
                })
            }
            <View style={styles.tableRow}>
                <View style={styles.tableRowColOne}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColTwo}>
                    <Text style={styles.boldText}>
                    </Text>
                </View>
                <View style={styles.tableRowColThree}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColFour}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColFive}>
                    <Text style={styles.boldText}>
                    </Text>
                </View>
                <View style={styles.tableRowColSix}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColSeven}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColEight}>
                    <Text style={styles.text}>
                        1,50,000.0
                    </Text>
                </View>
            </View>
            {emptyRow()}
            <View style={styles.tableRow}>
                <View style={styles.tableRowColOne}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={{ flex: 18, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'right', }}>
                    <Text style={styles.boldText}>
                        Central Tax
                    </Text>
                </View>
                <View style={styles.tableRowColThree}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColFour}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColFive}>
                    <Text style={styles.boldText}>
                    </Text>
                </View>
                <View style={styles.tableRowColSix}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColSeven}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColEight}>
                    <Text style={styles.boldText}>
                        13,500.00
                    </Text>
                </View>
            </View>
            <View style={styles.tableRow}>
                <View style={styles.tableRowColOne}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={{ flex: 18, borderRight: '1px solid grey', paddingBottom: '5px', textAlign: 'right', }}>
                    <Text style={styles.boldText}>
                        State Tax
                    </Text>
                </View>
                <View style={styles.tableRowColThree}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColFour}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColFive}>
                    <Text style={styles.boldText}>
                    </Text>
                </View>
                <View style={styles.tableRowColSix}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColSeven}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColEight}>
                    <Text style={styles.boldText}>
                        13,500.00
                    </Text>
                </View>
            </View>
            {emptyRow()}
            {emptyRow()}
            <View style={styles.tableRowWithBorder}>
                <View style={styles.tableRowColOneWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColTwoWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColThreeWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColFourWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColFiveWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColSixWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColSevenWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColEightWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
            </View>
            <View style={styles.tableRowWithBorder}>
                <View style={styles.tableRowColOneWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColTwoWithBorder}>
                    <Text style={styles.text}>
                        Total
                    </Text>
                </View>
                <View style={styles.tableRowColThreeWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColFourWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColFiveWithBorder}>
                    <Text style={styles.boldText}>
                        20 Nos
                    </Text>
                </View>
                <View style={styles.tableRowColSixWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColSevenWithBorder}>
                    <Text style={styles.text}>
                    </Text>
                </View>
                <View style={styles.tableRowColEightWithBorder}>
                    <Text style={styles.boldText}>
                        ₹ 1,77,000.0
                    </Text>
                </View>
            </View>
        </View>
        <View style={styles.belowTableTopText}>
            <Text style={styles.text}>Amount chargeable in words</Text>
            <Text style={styles.text}>E. & O.E</Text>
        </View>
        <View style={styles.belowTable}>
            <View style={styles.belowTableRight}>
                <Text style={styles.boldText}>INR One Lakh Seventy Seven Thousand Only</Text>
            </View>
            <View style={styles.belowTableLeft}>
                <Text style={styles.text}>Company's Bank Details</Text>
                <View style={styles.bankDetails}>
                    <View style={styles.bankDetail}>
                        <View style={styles.bankDetailsKey}>
                            <Text style={styles.text}>Bank Name</Text>
                        </View>
                        <View style={styles.bankDetailsValue}>
                            <Text style={styles.boldText}>ICICI Bank</Text>
                        </View>
                    </View>
                    <View style={styles.bankDetail}>
                        <View style={styles.bankDetailsKey}>
                            <Text style={styles.text}>Account Number</Text>
                        </View>
                        <View style={styles.bankDetailsValue}>
                            <Text style={styles.boldText}>1234567890</Text>
                        </View>
                    </View>
                    <View style={styles.bankDetail}>
                        <View style={styles.bankDetailsKey}>
                            <Text style={styles.text}>IFSC Code</Text>
                        </View>
                        <View style={styles.bankDetailsValue}>
                            <Text style={styles.boldText}>ICICI000041</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.bottomLeftTable}>
                    <View style={styles.bottomLeftTableTop}>
                        <Text style={styles.boldText}>For Max Electronics</Text>
                    </View>
                    <View style={styles.bottomLeftTableBottom}>
                        <Text style={styles.text}>Authorised Signatory</Text>
                    </View>
                </View>
            </View>
        </View>
    </View>
)

const emptyRow = () => (
    <View style={styles.tableRow}>
        <View style={styles.tableRowColOne}>
            <Text style={styles.text}>
            </Text>
        </View>
        <View style={styles.tableRowColTwo}>
            <Text style={styles.boldText}>
            </Text>
        </View>
        <View style={styles.tableRowColThree}>
            <Text style={styles.text}>
            </Text>
        </View>
        <View style={styles.tableRowColFour}>
            <Text style={styles.text}>
            </Text>
        </View>
        <View style={styles.tableRowColFive}>
            <Text style={styles.boldText}>
            </Text>
        </View>
        <View style={styles.tableRowColSix}>
            <Text style={styles.text}>
            </Text>
        </View>
        <View style={styles.tableRowColSeven}>
            <Text style={styles.text}>
            </Text>
        </View>
        <View style={styles.tableRowColEight}>
            <Text style={styles.text}>
            </Text>
        </View>
    </View>
)

export default MyDocument;